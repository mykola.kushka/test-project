window.bootstrap = require('../out/lib/bootstrap.bundle.min');

import { App } from '../components/app';

new App().init();
