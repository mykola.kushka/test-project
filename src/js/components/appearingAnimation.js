'use strict';

export function appearingAnimation() {
  // You can change this class to specify which counters are going to behave as counters.
  const counters = document.querySelectorAll('.scroll-counter');

  counters.forEach((item) => {
    // Add new attributes to the counters with the '.scroll-counter' HTML class
    item.counterAlreadyFired = false;
    item.counterSpeed = item.getAttribute('data-counter-time') / 45;
    item.counterTarget = +item.innerText;
    item.counterCount = 0;
    item.counterStep = item.counterTarget / item.counterSpeed;

    item.updateCounter = () => {
      item.counterCount = item.counterCount + item.counterStep;
      item.innerText = Math.ceil(item.counterCount);

      if (item.counterCount < item.counterTarget) {
        setTimeout(item.updateCounter, item.counterSpeed);
      } else {
        item.innerText = item.counterTarget;
      }
    };
  });

  // Function to determine if an element is visible in the web page
  const isElementVisible = (el) => {
    let scroll = window.scrollY || window.pageYOffset;
    let boundsTop = el.getBoundingClientRect().top + scroll;
    let viewport = {
      top: scroll,
      bottom: scroll + window.innerHeight,
    };
    let bounds = {
      top: boundsTop,
      bottom: boundsTop + el.clientHeight,
    };
    return (
      (bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom) ||
      (bounds.top <= viewport.bottom && bounds.top >= viewport.top)
    );
  };

  // Funciton that will get fired uppon scrolling
  const handleScroll = () => {
    counters.forEach((item, id) => {
      if (true === item.counterAlreadyFired) return;
      if (!isElementVisible(item)) return;
      item.updateCounter();
      item.counterAlreadyFired = true;
    });

    els_inViewport.forEach((el) => {
      if (!isElementVisible(el)) return;
      el.classList.add('is-inViewport');
    });
  };

  const els_inViewport = document.querySelectorAll('[data-inviewport]');

  // Fire the function on scroll
  if (els_inViewport.length > 0 || counters.length > 0)
    window.addEventListener('scroll', handleScroll);

  if (els_inViewport.length > 0 || counters.length > 0) handleScroll();
}
