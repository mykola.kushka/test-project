'use strict';

export function headerMenu() {
  const navbarToggler = document.querySelector('#mainMenuBtn');
  const mainMenu = document.querySelector('#mainMenu');
  const headerMain = document.querySelector('#headerMain');
  const body = document.querySelector('body');

  navbarToggler.addEventListener('click', () => {
    if (navbarToggler.getAttribute('aria-expanded') === 'true') {
      headerMain.classList.add('header-main-menu-opened');
      body.classList.add('main-menu-opened');
    } else {
      headerMain.classList.remove('header-main-menu-opened');
      body.classList.remove('main-menu-opened');
    }
  });

  if (window.innerWidth > 991) {
    mainMenu.addEventListener('hidden.bs.collapse', () => {
      headerMain.classList.remove('header-main-menu-opened');
      body.classList.remove('main-menu-opened');
    });
    mainMenu.addEventListener('shown.bs.collapse', () => {
      headerMain.classList.add('header-main-menu-opened');
      body.classList.add('main-menu-opened');
    });

    document.addEventListener('click', (event) => {
      if (!event.target.closest('.navbar-collapse')) {
        const body = document.querySelector('body');
        if (body.classList.contains('main-menu-opened')) {
          const parentMenuItem = document.querySelector(
            '.header-main-menu-parent[aria-expanded="true"]'
          );
          parentMenuItem.click();
        }
      }
    });
  }
}
