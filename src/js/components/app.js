'use strict';

import { headerSticky } from './header';
import { headerMenu } from './headerMenu';
import { appearingAnimation } from './appearingAnimation';

export class App {
  init() {
    document.addEventListener('DOMContentLoaded', () => {
      headerMenu();
      headerSticky();
      appearingAnimation();
    });
  }
}
