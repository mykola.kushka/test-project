const path = require('path');
const glob = require('glob');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const Autoprefixer = require('autoprefixer');
const MQPacker = require('css-mqpacker');
const CssNano = require('cssnano');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const webpack = require('webpack');

let mode;
let isDev;
const devMode = 'development';
const paths = {
  app: path.resolve(__dirname, './src'),
  build: path.resolve(__dirname, './public'),
  fonts: './src/fonts',
  images: './src/images',
};

const extraPlugins = [];
const extraRules = [];

const generateHtmlPlugins = (templateDir) => {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
  templateFiles.forEach((item) => {
    const parts = item.split('.');
    const name = parts[0];
    const extension = parts[1];

    const fileName = isDev ? `${name}.html` : `${name}.html`;
    extraPlugins.push(
      new HtmlWebpackPlugin({
        filename: fileName,
        template: path.resolve(
          __dirname,
          `${templateDir}/${name}.${extension}`
        ),
        inject: false,
        minify: false,
      })
    );
  });
};

const generateJSFiles = (pattern) =>
  glob.sync(pattern).reduce((acc, path) => {
    const entry = path
      .replace('./src/', '')
      .replace('.js', '')
      .replace('out/', '');
    acc[entry] = paths.app + path.replace('./src', '');
    return acc;
  }, {});

const generateStyleFiles = (pattern) =>
  glob.sync(pattern).reduce((acc, path) => {
    const entry =
      'css/' + path.replace('./src/scss/out/', '').replace('.scss', '');
    acc[entry] = paths.app + path.replace('./src', '');
    return acc;
  }, {});

const entries = {
  ...generateJSFiles('./src/js/out/**/*.js'),
  ...generateStyleFiles('./src/scss/out/**/*.scss'),
};

const mainConfig = () => {
  isDev = mode === devMode;
  return {
    mode: isDev ? 'development' : 'production',
    entry: entries,
    output: {
      filename: '[name].js',
      path: paths.build,
      publicPath: '/',
    },
    devServer: {
      port: 9000,
      host: '127.0.0.1',
      disableHostCheck: true,
    },
    optimization: {
      minimize: !isDev,
      minimizer: [
        new TerserPlugin({
          test: /\.js/i,
          sourceMap: isDev,
          extractComments: true,
          terserOptions: {
            compress: {
              drop_console: true,
              drop_debugger: true,
            },
          },
        }),
      ],
    },
    plugins: [
      new CleanWebpackPlugin(),
      new FixStyleOnlyEntriesPlugin(),
      new CopyPlugin([
        {
          from: paths.images,
          to: paths.build + '/images',
        },
      ]),
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
    ].concat(extraPlugins),
    devtool: isDev ? 'source-map' : '',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: ['@babel/plugin-proposal-class-properties'],
            },
          },
        },
        {
          test: /\.html$/,
          include: path.resolve(__dirname, 'src/html/templates'),
          use: ['raw-loader'],
        },
        {
          test: /\.(scss)$/i,
          include: path.resolve(__dirname, 'src/scss'),
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '../',
              },
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: isDev,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                sourceMap: isDev,
                plugins: [
                  CssNano({
                    presets: [
                      'default',
                      {
                        discardComments: {
                          removeAll: true,
                        },
                      },
                    ],
                  }),
                  MQPacker(),
                  Autoprefixer(),
                ],
              },
            },

            {
              loader: 'sass-loader',
              options: {
                sourceMap: isDev,
              },
            },
          ],
        },
        {
          test: /\.(png|jpg|svg)$/,
          exclude: path.resolve(__dirname, './src/fonts'),
          loader: 'file-loader',
          options: {
            context: path.resolve(__dirname, 'src'),
            name: '[path][name].[ext]',
          },
        },
        {
          test: /\.(ttf|eot|woff|woff2|svg)$/,
          exclude: path.resolve(__dirname, './src/images'),
          loader: 'file-loader',
          options: {
            outputPath: './fonts',
            name: '[name].[ext]',
          },
        },
      ].concat(extraRules),
    },
  };
};

module.exports = (a, options) => {
  mode = options.mode;
  generateHtmlPlugins('./src/html/pages');
  return mainConfig();
};
